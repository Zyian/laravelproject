<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 5; $i++) {
            Product::create([
                'name' => $faker->title,
                'description' => $faker->paragraph
            ]);
        }
    }
}
