@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Product Listings</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    @foreach (App\Product::all() as $product)
                    <tr>
                        <td>{{$product->name}}</td>
                        <td>{{$product->description}}</td>
                        @guest
                        @else
                        <td><a href="{{route('product.edit', $product->id)}}" class="btn btn-warning float-right">Edit</a>
                        {!! Form::open(['method' => 'DELETE', 'style' =>'display:inline-block','route' => ['product.destroy', $product->id]]) !!}
                        <button type="submit" class="btn btn-danger">Delete</button>
                        {!! Form::close() !!}</td>
                        @endguest
                    </tr>
                    @endforeach
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
