@extends('layouts.app')

@section('content')
<div class="container">
<h2>{{ $product->name}}</h2>
<h4>Are you sure you want to delete this item?</h4><br />

@if (count($errors) > 0)
<div class="alert alert-danger">
    There were some problems adding the product.<br />
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

{!! Form::open([
    'method' => 'DELETE',
    'route' => ['product.destroy', $product->id]]
  ) !!}

<div class="form-group">
    {!! Form::submit('Delete Product', 
      array('class'=>'btn btn-danger'
    )) !!}
</div>
{!! Form::close() !!}
@endsection
