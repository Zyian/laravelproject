@extends('layouts.app')

@section('content')
<div class="container">
<h2>Product Addition</h2>
<h4>To add a new product, please fill in the form below</h4><br />

@if (count($errors) > 0)
<div class="alert alert-danger">
    There were some problems adding the product.<br />
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

{!! Form::open(
  array(
    'route' => 'product.store', 
    'class' => 'form')
  ) !!}

<div class="form-group">
    {!! Form::label('Product Name') !!}
    {!! Form::text('name', null, 
      array(
        'class'=>'form-control', 
        'placeholder'=>'Enter a product name'
      )) !!}
</div>

<div class="form-group">
    {!! Form::label('Product Description') !!}
    {!! Form::text('description', null, 
      array(
        'class'=>'form-control', 
        'placeholder'=>'Enter a product description'
      )) !!}
</div>

<div class="form-group">
    {!! Form::submit('Add Product', 
      array('class'=>'btn btn-primary'
    )) !!}
</div>
{!! Form::close() !!}
@endsection
