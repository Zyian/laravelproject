@extends('layouts.app')

@section('content')
<div class="container">
<h2>Update Product</h2>
<h4>Please update the product's fields.</h4><br />

@if (count($errors) > 0)
<div class="alert alert-danger">
    There were some problems adding the product.<br />
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

{!! Form::model($product, [
    'method' => 'PATCH',
    'route' => ['product.update', $product->id]
]) !!}

<div class="form-group">
    {!! Form::label('Product Name') !!}
    {!! Form::text('name', null, 
      array(
        'class'=>'form-control', 
        'placeholder'=>'$product->name'
      )) !!}
</div>

<div class="form-group">
    {!! Form::label('Product Description') !!}
    {!! Form::text('description', null, 
      array(
        'class'=>'form-control', 
        'placeholder'=>'$product->description'
      )) !!}
</div>

<div class="form-group">
    {!! Form::submit('Update Product', 
      array('class'=>'btn btn-primary'
    )) !!}
</div>
{!! Form::close() !!}
@endsection
