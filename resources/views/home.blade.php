@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome!</div>

                <div class="panel-body">
                    To view the products, Click the Products Link above.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
